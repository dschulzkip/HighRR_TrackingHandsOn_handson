/*!
 *  @file      event_sim.cpp
 *  @author    Alessio Piucci
 *  @brief     This macro generates the simulated events
 *  @returns   A .root output file with the generated events
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

//ROOT libraries
#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TRandom3.h"
#include "TEveTrack.h"
#include "TObjArray.h"

//custom libraries
#include "../include/cparser.h"
#include "../include/TFREvent.h"

using namespace std;

/*! print the instructions for this macro */
void print_instructions(){
  std::cout << std::endl;
  std::cout << "Mandatory options: " << std::endl;
  std::cout << "\t-e: number of events to simulate" << std::endl;
  std::cout << "\t-c: config file name" << std::endl;
  std::cout << "\t-o: output file name" << std::endl;
  std::cout << std::endl;
  exit(1);
}

int main(int argc, char **argv){

  std::cout << std::endl;
  std::cout << "---> event_sim" << std::endl;
  
  //--------------------------------------------//
  //  retrieve the job options from the parser  //
  //--------------------------------------------//

  CParser cmdline(argc, argv);

  //check the parsed options
  if(argc == 1)
    print_instructions();
  else{
    if((cmdline.GetArg("-e") == "") ||
       (cmdline.GetArg("-c") == "") || (cmdline.GetArg("-o") == ""))
      print_instructions();
  }

  //parse the options
  unsigned int num_events = std::stoi(cmdline.GetArg("-e"));
  std::string configFile_name = cmdline.GetArg("-c");
  std::string outFile_name = cmdline.GetArg("-o");

  //print the imported options
  std::cout << std::endl;
  std::cout << "num_events = " << num_events << std::endl;
  std::cout << "configFile_name = " << configFile_name << std::endl;
  std::cout << "outFile_name = " << outFile_name << std::endl;
  
  //open the config file
  boost::property_tree::ptree configFile;
  boost::property_tree::read_info(configFile_name, configFile);

  //open the output file
  TFile *outFile = new TFile(outFile_name.c_str(), "recreate");

  //---------------------//
  //  monitoring histos  //
  //---------------------//

  //event monitoring histos
  TH1D *h_moni_evtID = new TH1D("h_moni_evtID", "", 100., 0., 100000);
  TH1D *h_moni_nParticles = new TH1D("h_moni_nParticles", "", 100., 0., 500.);

  //vertex monitoring histos
  TH1D *h_moni_vx = new TH1D("h_moni_vx", "", 100., -100., 100.);
  TH1D *h_moni_vy = new TH1D("h_moni_vy", "", 100., -100., 100.);
  TH1D *h_moni_vz = new TH1D("h_moni_vz", "", 100., -100., 100.);

  //momentum monitoring histos
  TH1D *h_moni_px = new TH1D("h_moni_px", "", 100., -100000., 100000.);
  TH1D *h_moni_py = new TH1D("h_moni_py", "", 100., -100000., 100000.);
  TH1D *h_moni_pz = new TH1D("h_moni_pz", "", 100., -100000., 100000.);

  //charge monitoring histo
  TH1D *h_moni_charge = new TH1D("h_moni_charge", "", 10., -3., 3.);
  
  //-----------------------//
  //  simulate the events  //
  //-----------------------// 

  //I can simulate the events in different ways:
  //a) randomly, from uniform distributions
  //b) randomly, from input histos
  //c) ...?
  
  //a crosscheck variable, to make sure to generate the events
  //in one way only
  bool generation_completed = false;
  
  //events
  TFREvents *event_list = new TFREvents();
  event_list->SetOwner(kTRUE);
  
  //random, from uniform distributions
  if(configFile.get<unsigned int>("random_uniform")){
    
    if(generation_completed){
      std::cout << "Error: generating with random_uniform, but the generation is already completed." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //vertex boundaries
    double max_vx = configFile.get<double>("uniform.max_vx");
    double min_vx = configFile.get<double>("uniform.min_vx");
    
    double max_vy = configFile.get<double>("uniform.max_vy");
    double min_vy = configFile.get<double>("uniform.min_vy");
    
    double max_vz = configFile.get<double>("uniform.max_vz");
    double min_vz = configFile.get<double>("uniform.min_vz");
    
    //momentum boundaries
    double max_px = configFile.get<double>("uniform.max_px");
    double min_px = configFile.get<double>("uniform.min_px");
    
    double max_py = configFile.get<double>("uniform.max_py");
    double min_py = configFile.get<double>("uniform.min_py");
    
    double max_pz = configFile.get<double>("uniform.max_pz");
    double min_pz = configFile.get<double>("uniform.min_pz");
    
    //number of particles in the event
    unsigned int max_numParticles = configFile.get<unsigned int>("uniform.max_numParticles");
    unsigned int min_numParticles = configFile.get<unsigned int>("uniform.min_numParticles");

    if(max_numParticles < min_numParticles){
      std::cout << "Error: min_numParticles = " << min_numParticles
		<< ", max_numParticles = " << max_numParticles << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //define all the (pseudo-)random generators that we need
    TRandom3 *vx_random = new TRandom3();
    TRandom3 *vy_random = new TRandom3();
    TRandom3 *vz_random = new TRandom3();
    
    TRandom3 *px_random = new TRandom3();
    TRandom3 *py_random = new TRandom3();
    TRandom3 *pz_random = new TRandom3();
    
    TRandom3 *charge_random = new TRandom3();
    
    TRandom3 *nParticles_random = new TRandom3();

    //set the seeds
    vx_random->SetSeed(0);
    vy_random->SetSeed(1);
    vz_random->SetSeed(2);
    px_random->SetSeed(3);
    py_random->SetSeed(4);
    pz_random->SetSeed(5);
    charge_random->SetSeed(6);
    nParticles_random->SetSeed(7);
    
    //loop over the events to generate
    for(unsigned int i_event = 0; i_event < num_events; ++i_event){
      
      //set a new event
      TFREvent *event = new TFREvent();
      event->SetID(i_event);
      
      //set the number of particles of the event
      unsigned int nParticles = nParticles_random->Uniform(min_numParticles, max_numParticles);
      
      //std::cout << "evtID = " << event->GetID() << ", nParticles = " << nParticles << std::endl;
      
      //loop over the particles of the current event
      for(unsigned int i_particle = 0; i_particle < nParticles; ++i_particle){
	
	//set the charge
	int charge;
	
	if(charge_random->Uniform(-1, 1) > 0.)
	  charge = 1;
	else
	  charge = -1;

	//set the particle, with NULL propagator (the B field is not set yet, in this generator phase!
	TFRParticle *particle = new TFRParticle(TVector3(vx_random->Uniform(min_vx, max_vx),
							 vy_random->Uniform(min_vy, max_vy),
							 vz_random->Uniform(min_vz, max_vz)),
						TVector3(px_random->Uniform(min_px, max_px),
							 py_random->Uniform(min_py, max_py),
							 pz_random->Uniform(min_pz, max_pz)),
						493.7,  //let's use all charged kaons, for now
						charge);

	//set the particle ID
	particle->SetID(i_particle);
	
	/*
	//a debug output
	std::cout << "momentum = (" << (particle->GetMomentum())[0]
		  << ", " << (particle->GetMomentum())[1]
		  << ", " << (particle->GetMomentum())[2]
		  << ") MeV/c" << std::endl;
	*/
	
	//add the particle to the event
	event->AddGenParticle(particle);
	
      } //loop over particles
      
      //add the current event to the list of events
      event_list->Add(event);
      
    }  //loop over the events

    //event generation completed!
    generation_completed = true;
    
  }  //random, from uniform distributions
  
  //random, from input histos
  if(configFile.get<unsigned int>("random_histos")){
    
    if(generation_completed){
      std::cout << "Error: generating with random_histos, but the generation is already completed." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //open the input file containing the histos
    TFile *inFile = new TFile((configFile.get<std::string>("histos.inFile_name")).c_str(), "read");
    
    //get the vertex histos
    TH1D *h_vx = (TH1D*) inFile->Get((configFile.get<std::string>("histos.vx")).c_str());
    TH1D *h_vy = (TH1D*) inFile->Get((configFile.get<std::string>("histos.vy")).c_str());
    TH1D *h_vz = (TH1D*) inFile->Get((configFile.get<std::string>("histos.vz")).c_str());
    
    //get the momentum histos
    TH1D *h_px = (TH1D*) inFile->Get((configFile.get<std::string>("histos.px")).c_str());
    TH1D *h_py = (TH1D*) inFile->Get((configFile.get<std::string>("histos.py")).c_str());
    TH1D *h_pz = (TH1D*) inFile->Get((configFile.get<std::string>("histos.pz")).c_str());
    
    //get the nParticles histos
    TH1D *h_nParticles = (TH1D*) inFile->Get((configFile.get<std::string>("histos.nParticles")).c_str());

    //loop over the events to generate
    for(unsigned int i_event = 0; i_event < num_events; ++i_event){

      //set a new event
      TFREvent *event = new TFREvent();
      event->SetID(i_event);

      //set the number of particles of the event
      unsigned int nParticles = h_nParticles->GetRandom();
      
      //set a (pseudo-)random generator for the particle charge
      TRandom3 *charge_random = new TRandom3();

      std::cout << "nParticles = " << nParticles << std::endl;

      //loop over particles of the current event
      for(unsigned int i_particle = 0; i_particle < nParticles; ++i_particle){
	
	//set the charge
	int charge;

        if(charge_random->Uniform(-1, 1) > 0.)
          charge = 1;
	else
          charge = -1;

        //set the particle, with NULL propagator (the B field is not set yet, in this generator phase!
	TFRParticle *particle = new TFRParticle(TVector3(h_vx->GetRandom(),
							 h_vy->GetRandom(),
							 h_vz->GetRandom()),
						TVector3(h_px->GetRandom(),
							 h_py->GetRandom(),
							 h_pz->GetRandom()),
						493.7,  //let's use all charged kaons, for now
						charge);

	//set the particle ID
        particle->SetID(i_particle);
	
	//add the particle to the event
	event->AddGenParticle(particle);
	
      }  //loop over particles

      //add the current event to the list of events
      event_list->Add(event);
      
    }  //loop over the events
    
    //close the input file
    inFile->Close();
    
    //event generation completed!
    generation_completed = true;
    
  }  //random, from input distributions

  
  //-----------------------//
  //  write to the output  //
  //-----------------------//

  //write the events to the output file
  outFile->cd();
  event_list->Write("event_list", TObject::kSingleKey);
  
  //write the monitoring histos
  h_moni_evtID->Write("h_evtID");
  h_moni_nParticles->Write("h_nParticles");
  h_moni_vx->Write("h_vx");
  h_moni_vy->Write("h_vy");
  h_moni_vz->Write("h_vz");
  h_moni_px->Write("h_px");
  h_moni_py->Write("h_py");
  h_moni_pz->Write("h_pz");
  h_moni_charge->Write("h_charge");
  
  //write the output file
  outFile->Write();

  return 0;
}
