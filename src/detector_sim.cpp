/*!
 *  @file      detector_sim.cpp
 *  @author    Alessio Piucci
 *  @brief     This macro simulates the detector response to the events
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

//ROOT libraries
#include "TROOT.h"
#include "TFile.h"
#include "TEveTrack.h"
#include "TRandom3.h"

//custom libraries
#include "../include/cparser.h"
#include "../include/TFREvent.h"
#include "../include/TFRParticle.h"
#include "../include/TFRGeometry.h"
#include "../include/TFRPropagator.h"

using namespace std;

/*! print the instructions for this macro */
void print_instructions(){
  std::cout << std::endl;
  std::cout << "Mandatory options: " << std::endl;
  std::cout << "\t-i: input file name" << std::endl;
  std::cout << "\t-g: config file name containing the geometry" << std::endl;
  std::cout << "\t-o: output file name" << std::endl;
  std::cout << std::endl;
  exit(1);
}

int main(int argc, char **argv){

  std::cout << std::endl;
  std::cout << "---> detector_sim" << std::endl;
    
  //--------------------------------------------//
  //  retrieve the job options from the parser  //
  //--------------------------------------------//

  CParser cmdline(argc, argv);

  //check the parsed options
  if(argc == 1)
    print_instructions();
  else{
    if((cmdline.GetArg("-i") == "") || (cmdline.GetArg("-g") == "")
       || (cmdline.GetArg("-o") == ""))
      print_instructions();
  }

  //parse the options
  std::string inFile_name = cmdline.GetArg("-i");
  std::string configGeoFile_name = cmdline.GetArg("-g");
  std::string outFile_name = cmdline.GetArg("-o");

  //print the imported options
  std::cout << std::endl;
  std::cout << "inFile_name = " << inFile_name << std::endl;
  std::cout << "configGeoFile_name = " << configGeoFile_name << std::endl;
  std::cout << "outFile_name = " << outFile_name << std::endl;

  //open the output file
  TFile *outFile = new TFile(outFile_name.c_str(), "recreate");
  
  //---------------------------------------//
  //  set the geometry and the propagator  //
  //---------------------------------------//

  //create the detector geometry
  TFRGeometry *detector_geo = new TFRGeometry(configGeoFile_name);

  std::cout << "Geometry imported, TAG = " << detector_geo->GetTag() << std::endl;
  std::cout << "nLayers = " << detector_geo->GetNLayers() << std::endl;

  //dump the geometry
  detector_geo->DumpGeometry();
  
  //set the propagator
  TFRPropagator *propagator = new TFRPropagator(detector_geo, false);

  
  //-------------------------------//
  //  import the simulated events  //
  //-------------------------------//
  
  //open the input file
  TFile *inFile = new TFile(inFile_name.c_str(), "read");
  
  if(inFile == NULL){
    std::cout << "Error: the input file does not exist" << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //get a clone of the input events
  TFREvents *event_list = (TFREvents*) inFile->Get("event_list");
  
  //check for the existence of the input events
  if(event_list == NULL){
    std::cout << "Error: the input event list does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }

  //get a clone, I don't want to play with the original copy
  event_list = (TFREvents*) event_list->Clone();
  event_list->SetOwner(kTRUE);
  
  //some printouts
  std::cout << std::endl;
  std::cout << "number of events = " << event_list->GetEntries() << std::endl;
  std::cout << std::endl;

  
  //-------------------------------------------//
  //  propagate the particles in the detector  //
  //-------------------------------------------//

  //initialize the (pseudo-)random generators that I need
  TRandom3 *random_smearing = new TRandom3();   //hit smearing
  random_smearing->SetSeed(0);
  
  TRandom3 *random_noise = new TRandom3();   //detector noise
  random_noise->SetSeed(1);

  TRandom3 *random_ineff = new TRandom3();   //hit inefficiency
  random_ineff->SetSeed(1);

  TRandom3 *random_ms = new TRandom3();   //multiple scattering
  random_ms->SetSeed(1);

  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
  //but just this stupid way (that I even didn't used at my C course during my bachelor)
  TIter it_event(event_list);
  TFREvent *curr_event;

  //loop over events
  while((curr_event = (TFREvent*) it_event.Next())){

    //std::cout << "evtID = " << curr_event->GetID()
    //	      << ", number of generated particles = " << curr_event->GetNGenParticles() << std::endl;
    
    //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
    //but just this stupid way (that I even didn't used at my C course during my bachelor)
    TIter it_particle((TFRParticles*) curr_event->GetGenParticles());
    TFRParticle *curr_particle;

    //loop over particles
    while((curr_particle = (TFRParticle*) it_particle.Next())){

      //I'm only interested to charged particles:
      //neutral particles are not leaving any hit in the detector
      if(curr_particle->GetCharge() == 0)
	continue;
      
      //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
      //but just this stupid way (that I even didn't used at my C course during my bachelor)
      TIter it_layer((TFRLayers*) detector_geo->GetLayers());
      TFRLayer *curr_layer;

      
      //set the initial state with [vx, vy, tx, ty, q/p], z, 0
      double stateVectArray[5] = {curr_particle->GetVertex()[0],
						   curr_particle->GetVertex()[1],
						   curr_particle->GetTx(),
						   curr_particle->GetTy(),
						   curr_particle->GetQoP()};
      TFRState *temp_state = new TFRState(TVectorD(5,stateVectArray),
					  curr_particle->GetVertex()[2]);
      //loop over detector layers
      while((curr_layer = (TFRLayer*) it_layer.Next())){
	
	TFRMCHit *intersection = new TFRMCHit(*curr_layer);
	
	//check if the intersection is valid
	if(propagator->PropagateState(temp_state, curr_layer->GetZ())){
	  
	  //----------//
	  //  MC hit  //
	  //----------//
	  
	  //simple gaussian multiple scattering
	  if(detector_geo->GetMultipleScattStatus()){

	    //TASK: implement the multiple scattering here

	    //update the state with the new tx, ty
	    //temp_state->UpdateTxTy([a vector with the new tx, ty]);
	    
	  }  //if(detector_geo->GetMultipleScattStatus())
	  
	  //add the intersection to the vector of the hits of the particle
	  intersection->SetPosition(TVector3(temp_state->GetX(),
					     temp_state->GetY(),
					     curr_layer->GetZ()));
	  
	  curr_particle->AddHit(intersection);
	  
	  //-----------------------------------------//
	  //  digitization of MC hits into clusters  //
	  //-----------------------------------------//
	  
	  //smear the hit, to simulate the sensor resolution
	  double x_smeared = random_ineff->Gaus(intersection->GetPosition()[0],
						curr_layer->GetHitResolution());
	  double y_smeared = random_ineff->Gaus(intersection->GetPosition()[1],
					   curr_layer->GetHitResolution());
	  
	  //-------------------------------------------------------//
	  //  'discretize' the hit position in unities of sensors  //
	  //-------------------------------------------------------//
	  
	  //it's more convenient to do this in the reference system of the layer,
	  //where it's centered into the origin and not tilted
	  
	  //center the layer into the origin
	  double x_hit_ref = (x_smeared - curr_layer->GetPosition()[0]);
	  double y_hit_ref = (y_smeared - curr_layer->GetPosition()[1]);
	  
	  //now rotate
	  x_hit_ref = x_hit_ref*cos(curr_layer->GetDxDy()) + y_hit_ref*sin(curr_layer->GetDxDy());
	  y_hit_ref = -x_hit_ref*sin(curr_layer->GetDxDy()) + y_hit_ref*cos(curr_layer->GetDxDy());
	  
	  //compute the x_min and xmax of the cluster in the layer reference system
	  double x_min_ref = std::floor(x_hit_ref/curr_layer->GetXSegm()) * curr_layer->GetXSegm();
	  double x_max_ref = x_min_ref + curr_layer->GetXSegm();
	  
	  //compute the y_min and xmax of the cluster in the layer reference system
	  double y_min_ref = std::floor(y_hit_ref/curr_layer->GetYSegm()) * curr_layer->GetYSegm();
	  double y_max_ref = y_min_ref + curr_layer->GetYSegm();
	  
	  //move back the cluster coordinates in the detector reference
	  double x_min = (x_min_ref*cos(curr_layer->GetDxDy()) - y_min_ref*sin(curr_layer->GetDxDy())
			  + curr_layer->GetPosition()[0]);  
	  double x_max = (x_max_ref*cos(curr_layer->GetDxDy()) - y_max_ref*sin(curr_layer->GetDxDy())
			  + curr_layer->GetPosition()[0]);
	  
	  double y_min = (x_min_ref*sin(curr_layer->GetDxDy()) + y_min_ref*cos(curr_layer->GetDxDy())
			  + curr_layer->GetPosition()[1]);
	  double y_max = (x_max_ref*sin(curr_layer->GetDxDy()) + y_max_ref*cos(curr_layer->GetDxDy())
			  + curr_layer->GetPosition()[1]);
	  
	  //width of the cluster (in the intrinsic coordinates of the layer), only x
	  double x_width = x_max_ref-x_min_ref;
	  
	  //create the cluster
	  TFRCluster *cluster = new TFRCluster(*curr_layer,
					       x_min, x_max,
					       y_min, y_max, 
					       x_width,
					       intersection->GetPosition()[2]);  //z
	  
	  //add the cluster to the current particle
	  curr_particle->AddCluster(cluster);

	  //check if the current channelID was already fired before in this event
	  if(curr_event->FindCluster(cluster) != NULL){
	    
	    //too bad, the sensor was already fired:
	    //I cannot create a new cluster,
	    //but just adding a hit contribution to the already existing cluster
	    
	    ((TFRCluster *) curr_event->FindCluster(cluster))->AddHit(intersection);
	    
	  }  //check if the current channelID was already fired previously         
	  else{
	    
	    //new cluster! Add it to the event
	    cluster->AddHit(intersection);
	    
	    //add the cluster to the event
	    curr_event->AddCluster(cluster);
	    
	    //std::cout << "cluster added" << std::endl;
	  } //check if the current channelID was already fired previously
	  
	}  //valid intersection?
	else break;
	
	//FOR STUDENTS: IMPLEMENT NOISE
	
      }  //loop over the detector layers

    }  //loop over the particles
  }  //loop over the events
  

  //-----------------------//
  //  write to the output  //
  //-----------------------//
  
  outFile->cd();
  event_list->Write("event_list", TObject::kSingleKey);
  outFile->Write();
  
  return 0;
  
}
